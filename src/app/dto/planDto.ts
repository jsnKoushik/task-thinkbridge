export class PlanDto {
  id: number;
  name: string;
  price: string;
  description: string;
}
