import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor() { }

  productArray=[
    {
      id:1,
      name:'Apple Ipod 1',
      price:'90000',
      description:'Love music? You can keep more songs than ever on iPod touch, which now comes with up to 256GB of storage.1 Get an Apple Music subscription to stream over 70 million songs and download your favorites. Or load iPod touch with songs you love from the iTunes Store.'      
    },
    {
      id:2,
      name:'Apple Ipod 2',
      price:'90000',
      description:'Love music? You can keep more songs than ever on iPod touch, which now comes with up to 256GB of storage.1 Get an Apple Music subscription to stream over 70 million songs and download your favorites. Or load iPod touch with songs you love from the iTunes Store.'      
    },
    {
      id:3,
      name:'Apple Ipod 3',
      price:'90000',
      description:'Love music? You can keep more songs than ever on iPod touch, which now comes with up to 256GB of storage.1 Get an Apple Music subscription to stream over 70 million songs and download your favorites. Or load iPod touch with songs you love from the iTunes Store.'      
    },
    {
      id:4,
      name:'Apple Ipod 4',
      price:'90000',
      description:'Love music? You can keep more songs than ever on iPod touch, which now comes with up to 256GB of storage.1 Get an Apple Music subscription to stream over 70 million songs and download your favorites. Or load iPod touch with songs you love from the iTunes Store.'      
    }
  ]

  ///add Product
  addProduct(product){
    console.log('add')
    this.productArray.push(product)
  }

  ///deleteProduct 

  deleteProduct(id){
    this.productArray.splice(id,1)
  }

  ///Update Product
  updateProduct(item){
    let x = this.productArray.findIndex(prod=>prod.id == item.id)
    this.productArray[x]=item  
    console.log(this.productArray)
  }
}
