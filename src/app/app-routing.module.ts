import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddItemComponent } from '../app/components/add-item/add-item.component';
import { ViewItemComponent } from '../app/components/view-item/view-item.component';


const routes: Routes = [
  {
    path:'',component:ViewItemComponent
  },
  {
    path:'add',component:AddItemComponent
  },
  {
    path: 'edit/:id', component : AddItemComponent
    
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
