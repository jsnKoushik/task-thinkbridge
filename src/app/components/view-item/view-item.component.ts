import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-view-item',
  templateUrl: './view-item.component.html',
  styleUrls: ['./view-item.component.scss']
})
export class ViewItemComponent implements OnInit {

  products=[]
  index:any
  // declaring service using dependency injection //
  constructor(private product:ProductService,private router:Router) { }

  ngOnInit() {
    this.products=this.product.productArray
  }

  ///deleting the product by using product id
  delete(id){
    this.product.deleteProduct(id)
  }

  /// edit function for product by sending product id 
  edit(item){
    this.router.navigate(["edit", item.id]);
  }

  getindex(i){
    this.index = i
  }
}
