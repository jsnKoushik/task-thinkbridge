import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlanDto } from 'src/app/dto/planDto';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

  //creating new object from PlanDto//
  productDto= new PlanDto()
  id:any
  isEdit:boolean=false

  // declaring service using dependency injection //
  constructor(private productservice:ProductService, private router:Router, 
    private activatedRoute : ActivatedRoute) { }

  ngOnInit() {

    //getting id from query params
    this.id = this.activatedRoute.snapshot.params['id'];

    ///checking for id and getting the product based on ID
    if(this.id){
      this.isEdit=true
      let data = this.productservice.productArray.find(item=> item.id == this.id)
      console.log(data)
      this.productDto = data
    }
  }

  ///add item to list 

  addItem(form){
    if(form.valid){ //checking form validation
    if(this.id !=null){ 
      //if id is present then update the product 
      this.productservice.updateProduct(this.productDto)
      alert("Product Updated Successfully.")
      this.router.navigateByUrl('')
    }
    else{ 
    // if id = null then add product 
    let findId=this.productservice.productArray.find(item => item.id == this.productDto.id)
    if(findId == undefined){ 
      //checking for id if already exists
     this.productservice.addProduct(this.productDto)
     alert("Product Added Successfully.")
     this.router.navigateByUrl('')
    }
    else{
      alert("Product ID already exists")
    }
    }
  }
  }
}
